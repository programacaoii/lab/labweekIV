package semanaIV.atvI;

import java.util.ArrayList;
import java.util.List;

public class ArrayListApp {
	private List<Integer> lista1;
    private List<Integer> lista2;
    
    public ArrayListApp() {
        lista1 = new ArrayList<>();
        lista2 = new ArrayList<>();
    }

    public void adicionarElemento(int elemento) {
        lista1.add(elemento);
    }

    public void removerDuplicados() {
        List<Integer> listaAux = new ArrayList<>();
        for (int elemento : lista1) {
            if (!listaAux.contains(elemento)) {
                listaAux.add(elemento);
            } else {
                lista2.add(elemento);
            }
        }
        lista1 = listaAux;
    }

    public List<Integer> getLista1() {
        return lista1;
    }

    public List<Integer> getLista2() {
        return lista2;
    }
}
