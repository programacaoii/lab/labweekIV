package semanaIV.atvII;

public class Main {
	public static void main(String[] args) {
		// Exemplo de matriz de caracteres
		MatrizCaracteres matrizCaracteres = new MatrizCaracteres(3);
		
		/*
		matrizCaracteres.setElemento(0, 0, 'a');
		matrizCaracteres.setElemento(0, 1, 'b');
		matrizCaracteres.setElemento(0, 2, 'c');
		matrizCaracteres.setElemento(1, 0, 'd');
		matrizCaracteres.setElemento(1, 1, 'r');
		matrizCaracteres.setElemento(1, 2, 'f');
		matrizCaracteres.setElemento(2, 0, 'g');
		matrizCaracteres.setElemento(2, 1, 'h');
		matrizCaracteres.setElemento(2, 2, 'i');
		*/
		char c = 'a';
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				matrizCaracteres.setElemento(i,j,c);
				c++;
			}
		}
		System.out.println("Matriz quadrada de ordem 3 de caracteres");
		matrizCaracteres.listarDados();
		System.out.print("Diagonal Principal: ");
		matrizCaracteres.imprimirDiagonalPrincipal();
		System.out.println("Matriz Transposta:");
		matrizCaracteres.matrizTransposta();

		// Exemplo de matriz de inteiros
		MatrizInteiros matrizInteiros = new MatrizInteiros(3);
		
		
		
		matrizInteiros.setElemento(0, 0, 2);
		matrizInteiros.setElemento(0, 1, 3);
		matrizInteiros.setElemento(0, 2, 5);
		matrizInteiros.setElemento(1, 0, 7);
		matrizInteiros.setElemento(1, 1, 8);
		matrizInteiros.setElemento(1, 2, 9);
		matrizInteiros.setElemento(2, 0, 1);
		matrizInteiros.setElemento(2, 1, 5);
		matrizInteiros.setElemento(2, 2, 4);

		
		System.out.println("\nMatriz Original");
		matrizInteiros.listarDados();
		System.out.print("Diagonal Principal: ");
		matrizInteiros.imprimirDiagonalPrincipal();
		System.out.println("Matriz Transposta:");
		matrizInteiros.matrizTransposta();
	}
}
