# Laboratório - Semana IV: Programação

Bem-vindo ao laboratório da Semana IV de programaçãoII!.

## Atividades

Aqui estão os links para acessar os arquivos de cada atividade:

- <a href="https://gitlab.com/programacaoii/lab/labweekIV/-/tree/main/src/semanaIV/atvI" target="_blank">Atividade 1</a>
- <a href="https://gitlab.com/programacaoii/lab/labweekIV/-/tree/main/src/semanaIV/atvII" target="_blank">Atividade 2</a>

Clique nos links acima para abrir cada arquivo em uma nova aba do seu navegador.



