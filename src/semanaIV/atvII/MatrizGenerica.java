package semanaIV.atvII;


public class MatrizGenerica<T> {
    private Object[][] matriz;
    private int ordem;

    public MatrizGenerica(int ordem) {
        this.ordem = ordem;
        this.matriz = new Object[ordem][ordem];
        for (int i = 0; i < ordem; i++) {
            for (int j = 0; j < ordem; j++) {
                matriz[i][j] = null;
            }
        }
    }

    public void setElemento(int linha, int coluna, T elemento) {
        if (linha >= 0 && linha < ordem && coluna >= 0 && coluna < ordem) {
            matriz[linha][coluna] = elemento;
        }
    }

    public void listarDados() {
        for (int i = 0; i < ordem; i++) {
            for (int j = 0; j < ordem; j++) {
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void imprimirDiagonalPrincipal() {
        for (int i = 0; i < ordem; i++) {
            System.out.print(matriz[i][i] + " ");
        }
        System.out.println();
    }

    public void matrizTransposta() {
        for (int i = 0; i < ordem; i++) {
            for (int j = 0; j < ordem; j++) {
                System.out.print(matriz[j][i] + " ");
            }
            System.out.println();
        }
    }




}
