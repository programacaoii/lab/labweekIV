# Aplicativo OOP para revisar conceitos de ArrayList e LinkedList

Este é um aplicativo de linha de comando em Python que permite implementar duas listas: uma do tipo ArrayList e outra do tipo LinkedList. O objetivo é remover os elementos duplicados da lista de entrada e armazená-los em uma segunda lista.

## Funcionalidades

1. Criação de listas: Implementa duas listas, uma do tipo ArrayList e outra do tipo LinkedList, a partir dos dados de entrada fornecidos pelo usuário.

2. Remoção de duplicados: Remove elementos duplicados da lista de entrada e armazena-os em uma segunda lista.

3. Exibição das listas: Percorre e imprime as duas listas resultantes: uma lista sem elementos duplicados e outra lista contendo os elementos repetidos.

## Exemplo dos dados de entrada e saida

### Exemplo Inteiro
* Lista original: 1 -> 2 -> 3 -> 3 -> 4 -> 4 -> 5
* Lista sem duplicatas: 1 -> 2 -> 3 -> 4 -> 5
* Lista de elementos duplicados: 3 -> 4

#### Saida de Inteiros 
<img src="../img/output1.png" width="50%">

### Exemplo Caractere
* Lista original: a -> b -> b -> f -> c -> v -> x -> c
* Lista sem duplicatas: a -> b -> f -> c -> v-> x
* Lista de elementos duplicados: b -> c

#### Saida de Caracteres 
<img src="../img/output1.1.png" width="50%">

## Diagrama
<img src="../img/diagramaAtv1.png" width="50%">

