# Implementação de Matrizes e Genéricos

Neste projeto, será criada uma classe que representa uma matriz NxN e que pode aceitar diferentes tipos de dados, como Char, Int, Double e String. A classe terá funcionalidades para manipular a matriz, como listar seus dados, imprimir a diagonal principal e obter a matriz transposta.

## Funcionalidades

1. Criar matriz NxN: A classe irá criar uma matriz quadrada de ordem N, onde N é fornecido pelo usuário, e permitirá a inserção de diferentes tipos de dados.

2. Listar dados: Será possível percorrer a matriz da esquerda para a direita e de cima para baixo, imprimindo uma linha da matriz por vez.

3. Diagonal principal: A classe irá imprimir todos os elementos da diagonal principal, ou seja, os elementos que possuem índices iguais (linha == coluna).

4. Matriz transposta: A classe desfará a ordem da matriz, transformando as linhas em colunas e vice-versa, exibindo a matriz transposta.

## Exemplo de entrada e saída

Exemplo 1 - Matriz de caracteres:

Entrada: a b c d r f g h i

Saída:

<img src="../img/output2.1.png" width="70%"><br>


Exemplo 2 - Matriz de inteiros:

Entrada: 2 3 5 7 8 9 1 5 4

Saída:

<img src="../img/output2.png" width="70%">

## Diagrama
<img src="../img/diagramaAtv2.png" width="70%">