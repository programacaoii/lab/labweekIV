package semanaIV.atvI;

public class Main {

	public static void main(String[] args) {
		ArrayListApp arrayListApp = new ArrayListApp();
		LinkedListApp linkedListApp = new LinkedListApp();

	
		arrayListApp.adicionarElemento(1);
		arrayListApp.adicionarElemento(2);
		arrayListApp.adicionarElemento(3);
		arrayListApp.adicionarElemento(3);
		arrayListApp.adicionarElemento(4);
		arrayListApp.adicionarElemento(4);
		arrayListApp.adicionarElemento(5);
		
		System.out.println("--------------------------------------------------");
		System.out.println("ArrayList:");
		System.out.println("Lista Original: " + arrayListApp.getLista1());
		arrayListApp.removerDuplicados();
		System.out.println("Resultado (Liata 1): " + arrayListApp.getLista1());
		System.out.println("Filtrados (Lista 2): " + arrayListApp.getLista2());
		System.out.println("--------------------------------------------------");
		
		linkedListApp.adicionarElemento('a');
		linkedListApp.adicionarElemento('b');
		linkedListApp.adicionarElemento('b');
		linkedListApp.adicionarElemento('f');
		linkedListApp.adicionarElemento('c');
		linkedListApp.adicionarElemento('v');
		linkedListApp.adicionarElemento('x');
		linkedListApp.adicionarElemento('c');
		
		System.out.println("\n\n--------------------------------------------------");
		System.out.println("LinkedList:");
		System.out.println("Lista Original: " + linkedListApp.getLista1());
		linkedListApp.removerDuplicados();
		System.out.println("Resultado (LinkedList): " + linkedListApp.getLista1());
		System.out.println("Filtrados (LinkedList): " + linkedListApp.getLista2());
		System.out.println("--------------------------------------------------");
	}

}
