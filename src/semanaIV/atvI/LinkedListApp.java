package semanaIV.atvI;
import java.util.LinkedList;
import java.util.List;

public class LinkedListApp {
    private List<Character> lista1;
    private List<Character> lista2;

    public LinkedListApp() {
        lista1 = new LinkedList<>();
        lista2 = new LinkedList<>();
    }

    public void adicionarElemento(char elemento) {
        lista1.add(elemento);
    }

    public void removerDuplicados() {
        List<Character> listaAux = new LinkedList<>();
        for (char elemento : lista1) {
            if (!listaAux.contains(elemento)) {
                listaAux.add(elemento);
            } else {
                lista2.add(elemento);
            }
        }
        lista1 = listaAux;
    }

    public List<Character> getLista1() {
        return lista1;
    }

    public List<Character> getLista2() {
        return lista2;
    }
}

